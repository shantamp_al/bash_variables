# Hello_program version 2
# make a program that says hello to three people at once

# description
# the program will receive 3 names, and say hello to all 3.
# we should use good variables names: first_person, second_person, third_person

#echo $1
#echo $2
#echo $3

#psudo code
# I want to capture 3 arguments ad reassign them to their variable names

first_person=$1
second_person=$2
third_person=$3

# I want to call the variable name and say hello

echo "hello $first_person!"
echo "hello $second_person!"
echo "hello $third_person!"

