# Condition evaluate if somehting is results in true or false
# This allows us to control flow - meaning our code with take different actions depending on our answer

#syntax
# if ((<condition))
# then
    # block of code
# fi

#example

COUNTER=0

if (($COUNTER > 5))
then 
    echo "counter is smaller than 5!"
else
    echo "counter is larger than 5"
fi
