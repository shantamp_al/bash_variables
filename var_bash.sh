## BASH script for testing Arguments and User Input

```bash
echo "\$1"
echo "\" \" "
echo $1
echo $1
```

# You can assign a variable on your terminal.

# Examle
$ books="Harry Potter"
$ echo $books
> Harry Potter

# Interpolation of variable into a string

echo "this is argument 1 $1"
echo "this is argument 1 $2"
echo "this is argument 1 $3"
echo "this is argument 1 $4" "will you break?"

# Also single quotes protect double quotes and double quotes protect signle quotes

echo "I'm a cool guy"
echo 'This is a double quote """"'