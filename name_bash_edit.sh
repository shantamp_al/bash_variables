# Hello Program
# make a little program that says hello
# topics covered:
    # arguments
    # variables
    # interpolation

# Description:
# make a bash script that takes in 1 argument that is a name and says hello to that person.
# We should assign the argument a meaningful variable name like person_name

# I want to capture one argument, when the script is called
# We do this using $1
#echo $1

# assign the variable person_name to the first incoming argument
person_name=$1

# I want to echo back hello + name of the person
echo "hello $person_name!"
