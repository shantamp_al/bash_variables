# loops in bash.

#for x in item item2 banana bread milk butter
#do
#echo "you need to buy $x"
#done

## Make a new list, with your favourite movies and print it out.
## Hint: don't just hard code - use a variable to count.
## Start a counter OUTSIDE the loop
## In the loop's block, increment the counter.

## Make a new list of best restaurants and print it our in this format

COUNTER=0
for restaurant in "Honest Burger" "Burger and Lobster" "Wagamamas"
do
COUNTER=$ (expr $COUNTER + 1)
echo "$COUNTER - $restaurant"
done

### Using arguments with for loops
# $* means any number of argument passed to script

for arg in $*
do 
echo "this was one of your arguments: $arg"
done