# Bash Script, Variables, Arguments and Conditions

This will be a small demo / code along for use to learn how to use variables, arguments and conditions.

By the end we'll have a small script that uses these to install nginx on an ubuntu machine when given an IP as an argument.

We'll talk about th difference between arguments and variables.

We'll also look into using conditions to help us run our script - using control flow. Also handle errors.

## To cover

- Script basics
- Running script
- Escape notations
- Arguments
- Variables and user input
- Conditions

### Script Basics

Script is several bash commands written in a file that a bash interpreter can execute. This is useful to automate tasks and others.

### Running Script

To run a script, it must first be executable. Check it's permissions using ` $ ll ` or ` $ ls -l `. It should have 'x' to be executed.

Add the permission "execute' by ` chmod +x <filename.sh> `

You might need to do this in a remote machine as well when moving or creating files on the fly.

Also, runnign it remotely might be easier to call the file using a full bash command 

```bash
# To run the file remotely call it using bash
$ bash <path/to/file>

```

### Escape Notations

In Bash and other languages, you have certain characters that behave/signal important things other than the actual symbol. For example, `""` are used to outline a string of characters.

How do you print out / echo out some `"`?

*you use what is called an escape character. In bash this is the backslash \*

```bash

#Example
echo "\$1"
echo "\" \" "
```

### Argument in scripts

Arguments are data that a function can take in and use internally.

` $ touch <argument> `
or 
` $ mv <argument1> <argument2> `

In script if you want to use arguments, they are defin with $<number>. 

For example:

```bash
echo $1
echo $2



# Interpolation of variable into a string

echo "this is argument 1 $1"
echo "this is argument 1 $2"
echo "this is argument 1 $3"
echo "this is argument 1 $4" "will you break?"

# Also single quotes protect double quotes and double quotes protect signle quotes

echo "I'm a cool guy"
echo 'This is a double quote """"'

```
Imagine you want to make a function , or script that takes in any number of arguments and does soemthing?

You can use `$*` to represent all the given arguments in a list.

To explain the above we need loops!

### Loops

A loop is a block of code that runs five or more times. This can be useful if you hve repeatative tasks.

```bash

for x in [item item2 item3]
do
echo "block of code"
echo $x
done

```

In a loop, a program iterates of the iteratable object (usually a list) substituting in each iteration the 'x' for an object on the list








